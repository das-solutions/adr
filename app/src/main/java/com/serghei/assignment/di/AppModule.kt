package com.serghei.assignment.di

import androidx.appcompat.app.AppCompatActivity
import com.serghei.assignment.core.data.*
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.core.interactors.GetTargeting
import com.serghei.assignment.data.DefaultDataSource
import com.serghei.assignment.data.EmailPublishCampaignRepository
import com.serghei.assignment.data.InMemoryReviewRepository
import com.serghei.assignment.remote.API_CAMPAIGN_BASE_URL
import com.serghei.assignment.remote.CampaignService
import com.serghei.assignment.ui.campaign.CampaignsFragment
import com.serghei.assignment.ui.campaign.CampaignsViewModel
import com.serghei.assignment.ui.campaignreview.CampaignsReviewFragment
import com.serghei.assignment.ui.campaignreview.CampaignsReviewViewModel
import com.serghei.assignment.ui.channel.ChannelsFragment
import com.serghei.assignment.ui.channel.ChannelsViewModel
import com.serghei.assignment.ui.targeting.TargetingFragment
import com.serghei.assignment.ui.targeting.TargetingViewModel
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AppModule {
    @ObsoleteCoroutinesApi
    fun getModule() = listOf(
        uiModule,
        dataModule,
        networkModule
    )
}

/** application ui module */
private val uiModule = module {
    // targeting module
    scope<TargetingFragment> {
        viewModel {
            TargetingViewModel(GetTargeting(get()), get())
        }
    }

    // channel module
    scope<ChannelsFragment> {
        viewModel {
            ChannelsViewModel(get(), get())
        }
    }

    // campaign module
    scope<CampaignsFragment> {
        viewModel { (channel: Channel) ->
            CampaignsViewModel(channel, get(), get())
        }
    }

    // campaign review module
    scope<CampaignsReviewFragment> {
        viewModel { (activity: AppCompatActivity) ->
            CampaignsReviewViewModel(
                get(), EmailPublishCampaignRepository(
                    activity
                )
            )
        }
    }
}

/** data module (datasource, repository) */
@ObsoleteCoroutinesApi
private val dataModule = module {
    single<TargetingDataSource> { get<DefaultDataSource>() }
    single<ChannelDataSource> { get<DefaultDataSource>() }
    single<CampaignDataSource> { get<DefaultDataSource>() }
    single { DefaultDataSource(get()) }

    single { TargetingRepository(get()) }
    single { ChannelRepository(get()) }
    single { CampaignRepository(get()) }
    single<CampaignReviewRepository> { InMemoryReviewRepository() }
    single<PublishCampaignReviewRepository> { (act: AppCompatActivity) ->
        EmailPublishCampaignRepository(act)
    }
}

/** network module*/
private val networkModule = module {
    single<CampaignService> { get<Retrofit>().create(CampaignService::class.java) }
    single {
        Retrofit.Builder()
            .baseUrl(API_CAMPAIGN_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}