package com.serghei.assignment.core.data

import com.serghei.assignment.core.domain.CampaignForReview

interface PublishCampaignReviewRepository {
    suspend fun publishCampaign(campaignForReview: CampaignForReview)
}