package com.serghei.assignment.core.domain

import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class Campaign(
    val id: String,
    val name: String,
    val monthlyFees: Int,
    val monthlyFeesCurrency: String,
    val attributes: List<String>
) : Serializable