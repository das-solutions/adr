package com.serghei.assignment.core.domain

import androidx.annotation.Keep

@Keep
data class CampaignForReview(
    var selectedTargeting: List<Targeting> = emptyList(),
    val campaignByChannel: MutableMap<Channel, Campaign> = hashMapOf()
) {
    fun getCampaignsForReview() = campaignByChannel.values

    fun getCampaignsForReview(channel: Channel) = campaignByChannel[channel]

    fun getChannelsForReview() = campaignByChannel.keys

    fun isBlank() = selectedTargeting.isEmpty() && campaignByChannel.isEmpty()
}