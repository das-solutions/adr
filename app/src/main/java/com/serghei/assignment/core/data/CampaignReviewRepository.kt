package com.serghei.assignment.core.data

import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.core.domain.CampaignForReview
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.core.domain.Targeting

interface CampaignReviewRepository {
    suspend fun setTargetingList(targetingList: List<Targeting>)
    suspend fun addCampaignToReview(channel: Channel, campaign: Campaign)
    suspend fun removeCampaignFromReview(channel: Channel)
    suspend fun getCampaignReviewByChannel(channel: Channel): Campaign?
    suspend fun getCampaignForReview(): CampaignForReview?
    suspend fun resetCampaignReview()
}