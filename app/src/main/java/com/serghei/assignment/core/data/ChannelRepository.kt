package com.serghei.assignment.core.data

class ChannelRepository(private val channelDataSource: ChannelDataSource) {
    suspend fun getChannels(targetingId: String) = channelDataSource.getChannel(targetingId)
}