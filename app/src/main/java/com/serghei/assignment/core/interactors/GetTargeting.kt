package com.serghei.assignment.core.interactors

import com.serghei.assignment.core.data.TargetingRepository

class GetTargeting(private val targetingRepository: TargetingRepository) {
    suspend operator fun invoke() = targetingRepository.getTargeting()
}