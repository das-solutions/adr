package com.serghei.assignment.core.data

import com.serghei.assignment.core.domain.Channel

interface ChannelDataSource {
    suspend fun getChannel(targetingId: String): List<Channel>
}