package com.serghei.assignment.core.domain

import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class Targeting(
    val id: String,
    val name: String
) : Serializable
