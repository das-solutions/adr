package com.serghei.assignment.core.data

class CampaignRepository(private val campaignDataSource: CampaignDataSource) {
    suspend fun getCampaign(channelId: String) =
        campaignDataSource.getCampaign(channelId)
}