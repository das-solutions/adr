package com.serghei.assignment.core.data

import com.serghei.assignment.core.domain.Campaign

interface CampaignDataSource {
    suspend fun getCampaign(channelId: String): List<Campaign>
}