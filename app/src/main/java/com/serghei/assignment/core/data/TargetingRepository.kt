package com.serghei.assignment.core.data

class TargetingRepository(private val targetingDataSource: TargetingDataSource) {
    suspend fun getTargeting() =
        targetingDataSource.getTargeting()
}