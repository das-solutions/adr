package com.serghei.assignment.core.data

import com.serghei.assignment.core.domain.Targeting

interface TargetingDataSource {
    suspend fun getTargeting(): List<Targeting>
}