package com.serghei.assignment.navigation

import androidx.navigation.NavController
import com.serghei.assignment.utils.NavigationData

enum class AppNavigation : NavigationData {
    POP_BACK_STACK {
        override fun invokeNavigation(navController: NavController?) {
            navController?.popBackStack()
        }
    }
}