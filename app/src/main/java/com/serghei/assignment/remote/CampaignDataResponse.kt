package com.serghei.assignment.remote

import androidx.annotation.Keep

@Keep
data class CampaignDataResponse(
    val targeting: List<TargetingModel>,
    val channels: List<ChannelModel>
)

@Keep
data class TargetingModel(
    val id: String,
    val name: String,
    val channels: List<ChannelModel>
)

@Keep
data class ChannelModel(
    val id: String,
    val name: String,
    val campaigns: List<CampaignModel>? = null
)

@Keep
data class CampaignModel(
    val id: String,
    val name: String,
    val monthlyFees: Int,
    val currency: String,
    val attributes: List<String>
)