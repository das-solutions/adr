package com.serghei.assignment.remote

import retrofit2.http.GET
import retrofit2.http.Headers

const val API_CAMPAIGN_BASE_URL = "https://api.jsonbin.io/"
interface CampaignService {
    @Headers(
        "X-Master-Key: \$2b\$10\$2rLotNixnEhOYpmL7h/pJuqOxBHqp84348/0N9zEVycuK0Zl3AqhS",
        "X-Bin-Meta: false"
    )
    @GET("/v3/b/60e2cd069328b059d7b71a92")
    suspend fun getCampaignData(): CampaignDataResponse
}