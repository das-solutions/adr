package com.serghei.assignment.utils

import android.content.Context
import androidx.annotation.StringRes
import com.serghei.assignment.R

data class UiText(
    @StringRes val resourceTextMessage: Int = R.string.unknown_error,
    val textMessage: CharSequence? = null
) {
    fun getMessage(context: Context): CharSequence {
        return textMessage ?: context.getString(resourceTextMessage)
    }
}
