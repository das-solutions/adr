package com.serghei.assignment.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.serghei.assignment.R

private const val EMAIL_TYPE = "message/rfc822"
fun sendEmail(context: Context, emailAddress: String, subject: String, bodyText: String) {
    val i = Intent(Intent.ACTION_SEND)
    i.type = EMAIL_TYPE
    i.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailAddress))
    i.putExtra(Intent.EXTRA_SUBJECT, subject)
    i.putExtra(Intent.EXTRA_TEXT, bodyText)
    try {
        context.startActivity(
            Intent.createChooser(
                i,
                context.getString(R.string.export_email_chooser_title)
            )
        )
    } catch (ex: ActivityNotFoundException) {
        Toast.makeText(
            context,
            context.getString(R.string.export_error_no_email_client_found),
            Toast.LENGTH_SHORT
        ).show()
    }
}