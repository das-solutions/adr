package com.serghei.assignment.utils

import androidx.navigation.NavController

interface NavigationData {
    fun invokeNavigation(navController: NavController? = null)
}