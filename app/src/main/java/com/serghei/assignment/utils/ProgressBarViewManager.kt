package com.serghei.assignment.utils

import com.serghei.assignment.ContentToDisplay

interface ProgressBarViewManager {
    fun displayContentForType(contentToDisplay: ContentToDisplay)
}