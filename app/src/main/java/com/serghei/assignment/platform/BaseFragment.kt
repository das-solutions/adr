package com.serghei.assignment.platform

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.serghei.assignment.ContentToDisplay
import com.serghei.assignment.utils.ProgressBarViewManager
import org.koin.androidx.scope.ScopeFragment
import java.lang.IllegalStateException

abstract class BaseFragment<TViewBinding : ViewBinding> : ScopeFragment() {
    private var progressBarViewManager: ProgressBarViewManager? = null
    private var _binding: TViewBinding? = null
    val binding
        get() = _binding
            ?: throw IllegalStateException("This property is only valid between onCreateView and onDestroyView")

    abstract fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?): TViewBinding

    protected abstract fun configureBinding(savedInstanceState: Bundle?)

    protected abstract fun getVM(): BaseViewModel

    protected open fun onViewLoaded() {
        getVM().onViewLoaded()
    }

    protected open fun handleProgressVisibility(contentType: ContentToDisplay) {
        progressBarViewManager?.displayContentForType(contentType)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        progressBarViewManager = context as? ProgressBarViewManager
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureBinding(savedInstanceState)
        subscribeForViewModelEvents(getVM())
        onViewLoaded()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = createViewBinding(inflater, container)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun subscribeToUiProgressBarEvents(viewModel: BaseViewModel) {
        viewModel.contentLiveData.observe(viewLifecycleOwner) {
            handleProgressVisibility(it)
        }
    }

    private fun subscribeForNavigationEvents(viewModel: BaseViewModel) {
        viewModel.navigationLiveData.observe(viewLifecycleOwner) {
            findNavController().navigate(it)
        }

        viewModel.navigateDataLiveData.observe(viewLifecycleOwner) {
            it.invokeNavigation(findNavController())
        }
    }

    private fun subscribeForUiAlerts(viewModel: BaseViewModel) {
        viewModel.toastLiveData.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it.getMessage(requireContext()), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun subscribeForViewModelEvents(viewModel: BaseViewModel) {
        with(viewModel) {
            subscribeToUiProgressBarEvents(this)
            subscribeForNavigationEvents(this)
            subscribeForUiAlerts(this)
        }
    }
}