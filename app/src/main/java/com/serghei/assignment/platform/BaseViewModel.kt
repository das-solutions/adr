package com.serghei.assignment.platform

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.serghei.assignment.ContentToDisplay
import com.serghei.assignment.extensions.toUiText
import com.serghei.assignment.utils.NavigationData
import com.serghei.assignment.utils.SingleLiveEvent
import com.serghei.assignment.utils.UiText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseViewModel : ViewModel() {
    private val _contentLiveData = SingleLiveEvent<ContentToDisplay>()
    private val _navigationLiveData = SingleLiveEvent<NavDirections>()
    private val _navigateDataLiveData = SingleLiveEvent<NavigationData>()
    private val _toastLiveData = SingleLiveEvent<UiText>()

    val contentLiveData: LiveData<ContentToDisplay> = _contentLiveData
    val navigationLiveData: LiveData<NavDirections> = _navigationLiveData
    val navigateDataLiveData: LiveData<NavigationData> = _navigateDataLiveData
    val toastLiveData: LiveData<UiText> = _toastLiveData

    fun showProgressBar() {
        _contentLiveData.postValue(ContentToDisplay.LOADING_INDICATOR)
    }

    fun hideProgressBar() {
        _contentLiveData.postValue(ContentToDisplay.CONTENT)
    }

    protected fun navigateTo(navDirections: NavDirections) {
        _navigationLiveData.postValue(navDirections)
    }

    protected fun navigateTo(navigationData: NavigationData) {
        _navigateDataLiveData.postValue(navigationData)
    }

    fun showToast(toast: UiText) {
        _toastLiveData.postValue(toast)
    }

    internal open fun onViewLoaded() {

    }

    protected fun executeSuspend(
        coroutineScope: CoroutineScope = viewModelScope,
        exceptionHandlerBlock: ((Exception) -> Unit)? = null,
        block: suspend () -> Unit,
    ) {
        coroutineScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    block()
                }
            } catch (ex: Exception) {
                exceptionHandlerBlock?.invoke(ex) ?: showToast(ex.toUiText())
                hideProgressBar()
            }
        }
    }

    fun detachedScope() = CoroutineScope(Dispatchers.Default)
}