package com.serghei.assignment

import android.app.Application
import com.serghei.assignment.di.AppModule
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AssignmentApp : Application() {
    @ObsoleteCoroutinesApi
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@AssignmentApp)
            modules(AppModule.getModule())
        }
    }
}