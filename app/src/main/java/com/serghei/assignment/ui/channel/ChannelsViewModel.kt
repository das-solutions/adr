package com.serghei.assignment.ui.channel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serghei.assignment.core.data.CampaignReviewRepository
import com.serghei.assignment.core.data.ChannelRepository
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.platform.BaseViewModel

class ChannelsViewModel(
    private val channelRepository: ChannelRepository,
    private val reviewRepository: CampaignReviewRepository
) :
    BaseViewModel() {

    private val _channelsListLiveData = MutableLiveData<List<ChannelCampaignEntry>>()
    private val _enableReviewButton = MutableLiveData(false)

    val channelsListLiveData: LiveData<List<ChannelCampaignEntry>> = _channelsListLiveData
    val enableReviewButton: LiveData<Boolean> = _enableReviewButton

    override fun onViewLoaded() {
        super.onViewLoaded()
        loadContentData()
    }

    fun onReviewButtonClick() {
        navigateTo(ChannelsFragmentDirections.actionChannelsFragmentToCampaignReviewFragment())
    }

    fun onChannelItemClick(channel: Channel) {
        navigateTo(ChannelsFragmentDirections.actionChannelsFragmentToCampaignsFragment(channel))
    }

    private fun loadContentData() {
        executeSuspend {
            val rawChannels = mutableListOf<Channel>()
            val targetingIds =
                reviewRepository.getCampaignForReview()?.selectedTargeting?.map { it.id }
                    ?: emptyList()
            targetingIds.forEach {
                rawChannels.addAll(channelRepository.getChannels(it))
            }
            val channels = rawChannels.distinctBy {
                it.id
            }.sortedBy {
                it.name
            }.map {
                ChannelCampaignEntry(
                    channel = it,
                    selectedCampaign = reviewRepository.getCampaignReviewByChannel(it)
                )
            }
            _channelsListLiveData.postValue(channels)
            val enabled = reviewRepository.getCampaignForReview()
                ?.getCampaignsForReview()?.size?.let { it > 0 } ?: false
            _enableReviewButton.postValue(enabled)
        }
    }

    override fun onCleared() {
        executeSuspend(detachedScope()) {
            // clear selected campaigns
            reviewRepository.resetCampaignReview()
        }
        super.onCleared()
    }
}