package com.serghei.assignment.ui.campaign

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.serghei.assignment.databinding.FragmentCampaignBinding
import com.serghei.assignment.platform.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CampaignsFragment : BaseFragment<FragmentCampaignBinding>() {
    private val args: CampaignsFragmentArgs by navArgs()

    private val viewModel: CampaignsViewModel by viewModel {
        parametersOf(args.channel)
    }
    private var adapter: CampaignAdapter? = null
    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentCampaignBinding = FragmentCampaignBinding.inflate(inflater, container, false)

    override fun configureBinding(savedInstanceState: Bundle?) {
        viewModel.campaignDataLiveData.observe(viewLifecycleOwner) {
            adapter?.replaceListData(it)
        }
        viewModel.channelLiveData.observe(viewLifecycleOwner) {
            binding.tvChannelName.text = it.name
        }
        viewModel.updatedItemLiveData.observe(viewLifecycleOwner) {
            adapter?.toggleSelection(it)
        }
        adapter = CampaignAdapter {
            viewModel.onCampaignSelected(it)
        }

        val layoutManager = LinearLayoutManager(requireContext()).apply {
            orientation = LinearLayoutManager.VERTICAL
        }

        with(binding) {
            recyclerView.layoutManager = layoutManager
            recyclerView.setHasFixedSize(false)
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    layoutManager.orientation
                )
            )
            recyclerView.adapter = adapter
            btnNext.setOnClickListener {
                viewModel.onDoneButtonClick()
            }
        }
    }

    override fun getVM() = viewModel
}