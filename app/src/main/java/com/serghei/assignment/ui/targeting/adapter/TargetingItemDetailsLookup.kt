package com.serghei.assignment.ui.targeting.adapter

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView

class TargetingItemDetailsLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Long>() {
    override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
        val view = recyclerView.findChildViewUnder(e.x, e.y)
        return view?.run {
            (recyclerView.getChildViewHolder(view) as? TargetingListAdapter.ViewHolder)?.getItemDetails()
        }
    }

}