package com.serghei.assignment.ui.campaignreview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.serghei.assignment.databinding.FragmentReviewBinding
import com.serghei.assignment.platform.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CampaignsReviewFragment : BaseFragment<FragmentReviewBinding>() {

    private val viewModel: CampaignsReviewViewModel by viewModel {
        parametersOf(requireActivity())
    }
    private val adapter: ReviewAdapter by lazy {
        ReviewAdapter()
    }

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentReviewBinding = FragmentReviewBinding.inflate(inflater, container, false)

    override fun configureBinding(savedInstanceState: Bundle?) {
        viewModel.reviewData.observe(viewLifecycleOwner) {
            adapter.replaceAdapterData(it)
        }

        val layoutManager = LinearLayoutManager(requireContext()).apply {
            orientation = LinearLayoutManager.VERTICAL
        }

        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                layoutManager.orientation
            )
        )

        binding.btnNext.setOnClickListener {
            viewModel.onPublishReviewClick()
        }
    }

    override fun getVM() = viewModel

}