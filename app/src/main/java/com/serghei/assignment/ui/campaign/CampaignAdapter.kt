package com.serghei.assignment.ui.campaign

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.serghei.assignment.databinding.ListItemCampaignBinding

class CampaignAdapter(
    private val dataList: MutableList<CampaignItem> = mutableListOf(),
    val onItemClick: (CampaignItem) -> Unit
) :
    RecyclerView.Adapter<CampaignAdapter.ViewHolder>() {

    fun replaceListData(newData: List<CampaignItem>) {
        dataList.clear()
        dataList.addAll(newData)
        notifyDataSetChanged()
    }

    fun toggleSelection(item: CampaignItem) {
        dataList.forEachIndexed { index, it ->
            if (it != item) {
                if (it.selected) {
                    it.selected = false
                    notifyItemChanged(index)
                }
            } else {
                notifyItemChanged(index)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ListItemCampaignBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    fun getItem(position: Int): CampaignItem {
        return dataList[position]
    }

    override fun getItemCount() = dataList.size

    inner class ViewHolder(private val binder: ListItemCampaignBinding) :
        RecyclerView.ViewHolder(binder.root), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(model: CampaignItem) {
            binder.tvName.text = model.name
            binder.tvValue.text = model.description
            binder.imgCheck.isSelected = model.selected
        }

        override fun onClick(v: View?) {
            onItemClick(getItem(adapterPosition))
            notifyItemChanged(adapterPosition)
        }
    }

}