package com.serghei.assignment.ui.targeting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.serghei.assignment.databinding.FragmentTargetingBinding
import com.serghei.assignment.platform.BaseFragment
import com.serghei.assignment.ui.targeting.adapter.TargetingItemDetailsLookup
import com.serghei.assignment.ui.targeting.adapter.TargetingListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val TARGETING_SELECTION_ID = "targeting_selection_id"

class TargetingFragment : BaseFragment<FragmentTargetingBinding>() {
    private val viewModel: TargetingViewModel by viewModel()
    private var adapter: TargetingListAdapter? = null

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentTargetingBinding = FragmentTargetingBinding.inflate(inflater, container, false)

    override fun getVM() = viewModel

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter?.saveSelectionState(outState)
    }

    override fun configureBinding(savedInstanceState: Bundle?) {
        adapter = TargetingListAdapter()

        viewModel.targetingViewModel.observe(viewLifecycleOwner) {
            adapter?.replaceListData(it)
            savedInstanceState?.let { savedInstanceState ->
                adapter?.restoreSelectionState(savedInstanceState)
            }
        }

        viewModel.enabledNextButtonLiveData.observe(viewLifecycleOwner) {
            binding.btnNext.isEnabled = it
        }

        val layoutManager = LinearLayoutManager(requireContext()).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        binding.targetingList.layoutManager = layoutManager
        binding.targetingList.adapter = adapter
        binding.targetingList.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                layoutManager.orientation
            )
        )

        val tracker = SelectionTracker.Builder(
            TARGETING_SELECTION_ID,
            binding.targetingList,
            StableIdKeyProvider(binding.targetingList),
            TargetingItemDetailsLookup(binding.targetingList),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()
        adapter?.setupTracker(tracker)

        tracker.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                super.onSelectionChanged()
                viewModel.setSelectedItems(tracker.selection.toList())
            }
        })

        binding.btnNext.setOnClickListener {
            viewModel.onNextClick()
        }
    }
}