package com.serghei.assignment.ui.targeting.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.serghei.assignment.core.domain.Targeting
import com.serghei.assignment.databinding.ListItemTargetingBinding

class TargetingListAdapter(private val listData: MutableList<Targeting> = mutableListOf()) :
    RecyclerView.Adapter<TargetingListAdapter.ViewHolder>() {
    private var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    fun replaceListData(newListData: List<Targeting>, notify: Boolean = true) {
        listData.clear()
        listData.addAll(newListData)
        if (notify)
            notifyDataSetChanged()
    }

    fun setupTracker(tracker: SelectionTracker<Long>?) {
        this.tracker = tracker
    }

    fun saveSelectionState(bundle: Bundle) {
        tracker?.onSaveInstanceState(bundle)
    }

    fun restoreSelectionState(bundle: Bundle) {
        tracker?.onRestoreInstanceState(bundle)
    }

    override fun getItemId(position: Int): Long = listData[position].idAsLong()

    override fun getItemCount(): Int = listData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        tracker?.let {
            val model = listData[position]
            holder.bind(model, it.isSelected(model.idAsLong()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemTargetingBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class ViewHolder(private val binding: ListItemTargetingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: Targeting, isActivated: Boolean = false) {
            binding.tvName.text = model.name
            binding.root.isActivated = isActivated
            binding.checkbox.isSelected = isActivated
            itemView.setOnClickListener {

            }
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition

                override fun getSelectionKey(): Long = itemId

            }
    }

    private fun Targeting.idAsLong() = id.hashCode().toLong()
}