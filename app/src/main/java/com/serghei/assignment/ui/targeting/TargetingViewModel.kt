package com.serghei.assignment.ui.targeting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serghei.assignment.core.data.CampaignReviewRepository
import com.serghei.assignment.core.domain.Targeting
import com.serghei.assignment.core.interactors.GetTargeting
import com.serghei.assignment.platform.BaseViewModel

class TargetingViewModel(
    private val getTargeting: GetTargeting,
    private val reviewRepository: CampaignReviewRepository
) : BaseViewModel() {
    private val _targetingListLiveData = MutableLiveData<List<Targeting>>()
    private val _enabledNextButtonLiveData = MutableLiveData(false)

    val targetingViewModel: LiveData<List<Targeting>> = _targetingListLiveData
    val enabledNextButtonLiveData: LiveData<Boolean> = _enabledNextButtonLiveData
    private val selectedTargetingIds = mutableListOf<Long>()

    override fun onViewLoaded() {
        super.onViewLoaded()
        loadContent()
    }

    fun setSelectedItems(list: List<Long>) {
        selectedTargetingIds.clear()
        selectedTargetingIds.addAll(list)
        _enabledNextButtonLiveData.postValue(selectedTargetingIds.size > 0)
    }

    fun onNextClick() {
        executeSuspend {

            val selectedTargeting = getTargeting().filter {
                selectedTargetingIds.contains(it.id.hashCode().toLong())
            }
            reviewRepository.setTargetingList(selectedTargeting)

            navigateTo(TargetingFragmentDirections.actionTargetingFragmentToChannelsFragment())
            onCleared()
        }
    }

    private fun loadContent() {
        showProgressBar()
        executeSuspend {
            _targetingListLiveData.postValue(getTargeting.invoke())
            hideProgressBar()
        }
    }

    override fun onCleared() {
        selectedTargetingIds.clear()
        _enabledNextButtonLiveData.postValue(false)
        super.onCleared()
    }
}