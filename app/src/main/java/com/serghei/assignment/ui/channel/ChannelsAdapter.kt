package com.serghei.assignment.ui.channel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.serghei.assignment.databinding.ListItemChannelBinding

class ChannelsAdapter(
    val data: MutableList<ChannelCampaignEntry> = mutableListOf(),
    val onCampaignEntryClickListener: OnItemClickListener<ChannelCampaignEntry>? = null
) :

    RecyclerView.Adapter<ChannelsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemChannelBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun replaceListData(newData: List<ChannelCampaignEntry>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    private fun getItem(position: Int): ChannelCampaignEntry {
        return data[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount() = data.size

    inner class ViewHolder(private val viewBinding: ListItemChannelBinding) :
        RecyclerView.ViewHolder(viewBinding.root),
        View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        fun bind(campaignEntry: ChannelCampaignEntry) {
            val hasValue = campaignEntry.selectedCampaign != null
            viewBinding.tvName.text = campaignEntry.channel.name
            viewBinding.tvValue.text = campaignEntry.selectedCampaign?.name ?: ""
            itemView.isSelected = hasValue
        }

        override fun onClick(v: View?) {
            onCampaignEntryClickListener?.invoke(data[adapterPosition])
        }
    }
}

typealias OnItemClickListener<T> = (item: T) -> Unit