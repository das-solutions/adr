package com.serghei.assignment.ui.channel

import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.core.domain.Channel

data class ChannelCampaignEntry(
    val channel: Channel,
    val selectedCampaign: Campaign? = null
)