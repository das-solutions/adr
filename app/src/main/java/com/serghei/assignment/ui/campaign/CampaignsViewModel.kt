package com.serghei.assignment.ui.campaign

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.serghei.assignment.core.data.CampaignRepository
import com.serghei.assignment.core.data.CampaignReviewRepository
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.navigation.AppNavigation
import com.serghei.assignment.platform.BaseViewModel
import kotlinx.coroutines.launch

class CampaignsViewModel(
    private val channel: Channel,
    private val campaignRepository: CampaignRepository,
    private val campaignReviewRepository: CampaignReviewRepository
) : BaseViewModel() {

    val campaignDataLiveData = MutableLiveData<List<CampaignItem>>()
    val updatedItemLiveData = MutableLiveData<CampaignItem>()
    val channelLiveData = MutableLiveData<Channel>()

    init {
        channelLiveData.postValue(channel)
        executeSuspend {
            showProgressBar()
            val selectedCampaign = campaignReviewRepository.getCampaignReviewByChannel(channel)
            val campaignItems = campaignRepository.getCampaign(channelId = channel.id)
                .sortedBy {
                    it.monthlyFees
                }
                .map {
                    CampaignItem(
                        campaign = it,
                        it == selectedCampaign
                    )
                }
            campaignDataLiveData.postValue(campaignItems)
            hideProgressBar()
        }
    }

    fun onCampaignSelected(campaignItem: CampaignItem) {
        viewModelScope.launch {
            val rvCampaign = campaignReviewRepository.getCampaignReviewByChannel(channel)
            val remove = rvCampaign == campaignItem.campaign
            if (remove) {
                campaignReviewRepository.removeCampaignFromReview(channel)
            } else {
                campaignReviewRepository.addCampaignToReview(channel, campaignItem.campaign)
            }
            campaignItem.selected = !remove
            updatedItemLiveData.postValue(campaignItem)
        }
    }

    fun onDoneButtonClick() {
        navigateTo(AppNavigation.POP_BACK_STACK)
    }
}