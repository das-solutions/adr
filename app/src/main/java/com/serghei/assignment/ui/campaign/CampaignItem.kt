package com.serghei.assignment.ui.campaign

import androidx.annotation.Keep
import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.extensions.description

@Keep
data class CampaignItem(
    val campaign: Campaign,
    var selected: Boolean = false
) {
    val name = campaign.name
    val description: String = campaign.description()
}
