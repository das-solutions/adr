package com.serghei.assignment.ui.campaignreview

import androidx.annotation.Keep
import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.extensions.description

@Keep
data class ReviewItem(
    val channel: Channel,
    val campaign: Campaign,
    val details: String = campaign.description(),
    var expanded: Boolean = false
)
