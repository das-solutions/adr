package com.serghei.assignment.ui.channel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.serghei.assignment.databinding.FragmentChannelsBinding
import com.serghei.assignment.platform.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ChannelsFragment : BaseFragment<FragmentChannelsBinding>() {
    private val viewModel: ChannelsViewModel by viewModel {
        parametersOf(arrayListOf("1"))
    }

    private var adapter: ChannelsAdapter? = null

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentChannelsBinding = FragmentChannelsBinding.inflate(inflater, container, false)

    override fun configureBinding(savedInstanceState: Bundle?) {
        viewModel.channelsListLiveData.observe(viewLifecycleOwner) {
            adapter?.replaceListData(it)
        }
        viewModel.enableReviewButton.observe(viewLifecycleOwner) {
            binding.btnNext.isEnabled = it
        }
        adapter = ChannelsAdapter {
            viewModel.onChannelItemClick(channel = it.channel)
        }
        val layoutManager = LinearLayoutManager(requireContext()).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        with(binding) {
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    layoutManager.orientation
                )
            )
            btnNext.setOnClickListener {
                viewModel.onReviewButtonClick()
            }
        }
    }

    override fun getVM() = viewModel
}