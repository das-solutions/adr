package com.serghei.assignment.ui.campaignreview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serghei.assignment.core.data.CampaignReviewRepository
import com.serghei.assignment.core.data.PublishCampaignReviewRepository
import com.serghei.assignment.platform.BaseViewModel

class CampaignsReviewViewModel(
    private val reviewRepository: CampaignReviewRepository,
    private val publishRepository: PublishCampaignReviewRepository
) : BaseViewModel() {
    private val _reviewData = MutableLiveData<List<ReviewItem>>()
    val reviewData: LiveData<List<ReviewItem>> = _reviewData

    override fun onViewLoaded() {
        super.onViewLoaded()
        executeSuspend {
            showProgressBar()
            val reviewList = reviewRepository.getCampaignForReview()?.let { review ->
                review.getChannelsForReview().map {
                    ReviewItem(
                        channel = it,
                        campaign = review.getCampaignsForReview(it)!!
                    )
                }
            } ?: emptyList()
            _reviewData.postValue(reviewList)
            hideProgressBar()
        }
    }

    fun onPublishReviewClick() {
        executeSuspend {
            showProgressBar()
            reviewRepository.getCampaignForReview()?.let {
                publishRepository.publishCampaign(it)
                reviewRepository.resetCampaignReview()
                hideProgressBar()
                navigateTo(CampaignsReviewFragmentDirections.actionCampaignReviewFragmentToTargetingFragment())
            } ?: throw IllegalStateException("Not defined review data")

        }
    }
}