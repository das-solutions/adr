package com.serghei.assignment.ui.campaignreview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.serghei.assignment.databinding.ListItemReviewBinding

class ReviewAdapter(private val dataList: MutableList<ReviewItem> = mutableListOf()) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    fun replaceAdapterData(newData: List<ReviewItem>) {
        dataList.clear()
        dataList.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemReviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun getItem(position: Int) = dataList[position]

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount() = dataList.size

    inner class ViewHolder(private val binding: ListItemReviewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        @SuppressLint("SetTextI18n")
        fun bind(model: ReviewItem) {
            binding.tvName.text = "${model.channel.name} / ${model.campaign.name}"
            binding.tvValue.text = model.details
            binding.tvValue.isVisible = model.expanded
        }

        override fun onClick(v: View?) {
            getItem(adapterPosition).expanded = !getItem(adapterPosition).expanded
            notifyItemChanged(adapterPosition)
        }
    }
}