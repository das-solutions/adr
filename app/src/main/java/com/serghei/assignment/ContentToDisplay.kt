package com.serghei.assignment

enum class ContentToDisplay(
    val viewIdentifier: Int = 0
) {
    CONTENT(1),
    LOADING_INDICATOR(R.id.loading_content)
}