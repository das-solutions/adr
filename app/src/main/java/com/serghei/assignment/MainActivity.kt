package com.serghei.assignment

import android.os.Bundle
import android.widget.ViewSwitcher
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.serghei.assignment.extensions.displayContent
import com.serghei.assignment.utils.ProgressBarViewManager

class MainActivity : AppCompatActivity(), ProgressBarViewManager {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupActionBar()
    }

    override fun displayContentForType(contentToDisplay: ContentToDisplay) {
        findViewById<ViewSwitcher>(R.id.main_view_switcher).displayContent(contentToDisplay)
    }

    private fun setupActionBar(){
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setupWithNavController(navController, appBarConfiguration)
    }

}