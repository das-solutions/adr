package com.serghei.assignment.extensions

import com.serghei.assignment.utils.UiText

fun Throwable.toUiText(): UiText {
    return message?.let {
        UiText(textMessage = "Something wrong \n$it")
    } ?: UiText()
}