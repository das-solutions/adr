package com.serghei.assignment.extensions

import com.serghei.assignment.core.domain.Campaign

fun Campaign.description() = attributes.joinToString(separator = "\n")