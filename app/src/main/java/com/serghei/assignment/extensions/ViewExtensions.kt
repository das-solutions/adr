package com.serghei.assignment.extensions

import android.widget.ViewSwitcher
import androidx.core.view.children
import com.serghei.assignment.ContentToDisplay

fun ViewSwitcher.displayContent(contentToDisplay: ContentToDisplay) {
    val indexOfView = children.indexOfFirst { it.id == contentToDisplay.viewIdentifier }
    displayedChild = if (indexOfView < 0) {
        contentToDisplay.ordinal % childCount
    } else {
        indexOfView
    }
}