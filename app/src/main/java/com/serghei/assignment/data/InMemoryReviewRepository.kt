package com.serghei.assignment.data

import com.serghei.assignment.core.data.CampaignReviewRepository
import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.core.domain.CampaignForReview
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.core.domain.Targeting

class InMemoryReviewRepository : CampaignReviewRepository {
    private var campaignForReview = CampaignForReview()

    override suspend fun setTargetingList(targetingList: List<Targeting>) {
        campaignForReview.selectedTargeting = targetingList
    }

    override suspend fun addCampaignToReview(channel: Channel, campaign: Campaign) {
        campaignForReview.campaignByChannel[channel] = campaign
        campaignForReview.apply {
            campaignByChannel[channel] = campaign
        }
    }

    override suspend fun removeCampaignFromReview(channel: Channel) {
        campaignForReview.campaignByChannel.remove(channel)
    }

    override suspend fun getCampaignReviewByChannel(channel: Channel): Campaign? {
        return campaignForReview.campaignByChannel[channel]
    }

    override suspend fun getCampaignForReview(): CampaignForReview? {
        return campaignForReview.takeUnless { it.isBlank() }
    }

    override suspend fun resetCampaignReview() {
        campaignForReview = CampaignForReview()
    }
}