package com.serghei.assignment.data

import android.content.Context
import com.serghei.assignment.R
import com.serghei.assignment.core.data.PublishCampaignReviewRepository
import com.serghei.assignment.core.domain.CampaignForReview
import com.serghei.assignment.utils.sendEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val EMAIL_ADDRESS = "bogus@bogus.com"

class EmailPublishCampaignRepository(private val context: Context) :
    PublishCampaignReviewRepository {
    override suspend fun publishCampaign(campaignForReview: CampaignForReview) {
        val emailBodyText = generateEmailContentText(campaignForReview)
        withContext(Dispatchers.Main) {
            sendEmail(context, EMAIL_ADDRESS, context.getString(R.string.export_email_title), emailBodyText)
        }
    }

    private fun generateEmailContentText(review: CampaignForReview): String {
        val buffer = StringBuilder()

        // insert header
        buffer
            .append(headerTemplate())
            .append("\n")

        // insert body
        // targeting info
        review.selectedTargeting.takeIf {
            it.isNotEmpty()
        }?.let { targetingList ->
            buffer
                .append("Selected targeting: ")
                .append(
                    targetingList.joinToString(", ") { it.name }
                )
                .append("\n\n")
        }

        // channels info
        review.getChannelsForReview().forEach { channel ->
            val campaign = review.getCampaignsForReview(channel)
            buffer
                .append("------------------------------------------------------\n")
                .append(" Channel: ${channel.name}")
                .append("\n")
                .append("------------------------------------------------------\n")
                .append(" Campaign: ${campaign?.name}")
                .append("\n")
                .append(
                    campaign?.attributes?.joinToString(
                        separator = "\n",
                        transform = { attribute -> "  - $attribute" }) ?: "-"
                )
                .append("\n")
                .append("------------------------------------------------------\n")
                .append("\n")
        }

        // insert footer
        buffer
            .append("\n")
            .append(footerTemplate())

        return buffer.toString()
    }

    private fun headerTemplate(): String = ""

    private fun footerTemplate(): String = ""
}