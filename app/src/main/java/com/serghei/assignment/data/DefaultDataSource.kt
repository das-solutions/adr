package com.serghei.assignment.data

import com.serghei.assignment.core.data.CampaignDataSource
import com.serghei.assignment.core.data.ChannelDataSource
import com.serghei.assignment.core.data.TargetingDataSource
import com.serghei.assignment.core.domain.Campaign
import com.serghei.assignment.core.domain.Channel
import com.serghei.assignment.core.domain.Targeting
import com.serghei.assignment.remote.*
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.withContext

@ObsoleteCoroutinesApi
class DefaultDataSource(private val serviceApi: CampaignService) : TargetingDataSource,
    ChannelDataSource, CampaignDataSource {
    private val cacheContext = newSingleThreadContext("CacheDataThreadContext")
    private var cachedCampaignData: CampaignDataResponse? = null

    override suspend fun getTargeting(): List<Targeting> {
        return remoteData()
            .targeting
            .map {
                mapTargeting(it)
            }
    }

    override suspend fun getChannel(targetingId: String): List<Channel> {
        return remoteData()
            .targeting
            .find {
                it.id == targetingId
            }
            ?.channels
            ?.map {
                mapChannel(it)
            }
            ?: emptyList()
    }

    override suspend fun getCampaign(channelId: String): List<Campaign> {
        return remoteData()
            .channels
            .find {
                it.id == channelId
            }
            ?.campaigns
            ?.map {
                mapCampaign(it)
            }
            ?: emptyList()
    }

    private suspend fun remoteData(): CampaignDataResponse {
        return cachedCampaignData ?: kotlin.run {
            withContext(cacheContext) {
                val localData = serviceApi.getCampaignData()
                cachedCampaignData = localData
                localData
            }
        }
    }
}

// mappers
fun mapTargeting(source: TargetingModel): Targeting {
    return Targeting(
        id = source.id,
        name = source.name
    )
}

fun mapChannel(source: ChannelModel): Channel {
    return Channel(
        id = source.id,
        name = source.name
    )
}

fun mapCampaign(source: CampaignModel): Campaign {

    return Campaign(
        id = source.id,
        name = source.name,
        monthlyFees = source.monthlyFees,
        monthlyFeesCurrency = source.currency,
        attributes = source.attributes
    )
}
// ~mappers
